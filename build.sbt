name := "garmin-activity-extractor"

version := "0.0.1"

scalaVersion := "2.12.3"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
libraryDependencies += "com.github.scopt" % "scopt_2.12" % "3.7.0"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"

lazy val app = (project in file(".")).
  settings(
    name := "garmin-activity-extractor",
    mainClass := Some("Application"),
    version := "0.0.1",
    scalaVersion := "2.12.3"
  )