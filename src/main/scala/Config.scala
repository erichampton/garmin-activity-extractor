import java.nio.file.Path

/**
  * Configuration for Garmin Activity Extractor application.
  * @param inputFile The location of the garmin tcx file to be parsed.
  * @param outputLocation The location of where the extracted files will be stored.
  * @param splitBySize A flag that determines if the extraction is done by size.
  * @param maximumSize The maximum size the resulting files should be when using splitBySize.
  * @param splitByActivity A flag that determines if the extraction is done by activity.
  */
case class Config(
  inputFile: Path = null,
  outputLocation: Path = null,
  splitBySize: Boolean = false,
  maximumSize: Int = 25 * 1024 * 1024, // 25MB
  splitByActivity: Boolean = false
)