import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.LazyLogging
import scopt.{OptionParser, Read}

/**
  * Garmin Activity Extractor Application
  */
object Application extends App with LazyLogging {

  // Need to provide a `Read` for scopt to know about Paths.
  implicit val pathReader: Read[Path] = scopt.Read.reads(s => Paths.get(s))

  // cli options
  val parser = new OptionParser[Config]("garmin-data-extractor") {
    head("garmin-data-extractor", "v0.0.1")

    cmd("splitBySize").text("Split the activities in the input file by size.").action(
      (_, config) => config.copy(splitBySize = true)
    ).children(
      opt[Int]("maximumSize").text("Maximum file size when using `splitBySize`.").action(
        (size, config) => config.copy(maximumSize = size)
      )
    )

    cmd("splitByActivity").text("Split the activities in the input file by activity.").action(
      (_, config) => config.copy(splitByActivity = true)
    )

    arg[Path]("<input file>").required().text("garmin data file").action(
      (inputFile, config) => config.copy(inputFile = inputFile)
    ).validate {
      case inputFile if inputFile.toFile.isFile => Right()
      case _ => Left("Input file is required to be a file.")
    }

    arg[Path]("<output location>").required().text("output location").action(
      (outputLocation, config) => config.copy(outputLocation = outputLocation)
    ).validate {
      case outputLocation if outputLocation.toFile.isDirectory => Right()
      case _ => Left("Output location is required to be an existing directory.")
    }

    help("help").text("Print this usage text.")
  }

  // Parse the config options and run the provided action.
  parser.parse(args, Config()) match {
    case Some(config) =>
      val extractor = new GarminActivityExtractor(config.inputFile, config.outputLocation)

      config match {
        case c if c.splitByActivity => extractor.extractByActivity()
        case c if c.splitBySize => extractor.extractBySize(c.maximumSize)
        case _ =>
          logger.error("No command provided")
          System.exit(-1)
      }
    case None =>
      logger.error("Error parsing arguments")
      System.exit(-1)
  }
}