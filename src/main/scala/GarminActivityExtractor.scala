import java.nio.file.Path

import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable
import scala.xml._

/**
  * Companion object with constants for the garmin activity extractor.
  */
object GarminActivityExtractor {
  val ExtractBySizeFilePrefix = "garmin-data"
  val OutputFileExtension = ".tcx"
}

/**
  * The Garmin Activity Extractor.  This will parse a Garmin .tcx file and extract the activities
  * by either size or individual activity.  The extraction is done by keeping all other root
  * level nodes and replacing the activities within each file.
  * @param inputFile The location of the Garmin .tcx file.
  * @param outputLocation The location of where the resulting files are written.
  */
class GarminActivityExtractor(inputFile: Path, outputLocation: Path) extends LazyLogging {

  import GarminActivityExtractor._

  logger.info(s"Loading file ${inputFile.toString}")
  val xml: Elem = XML.loadFile(inputFile.toFile)

  // garmin tcx root nodes
  val folders: NodeSeq = xml \ "Folders"
  val activities: NodeSeq = xml \ "Activities"
  val workouts: NodeSeq = xml \ "Workouts"
  val courses: NodeSeq = xml \ "Courses"
  val author: NodeSeq = xml \ "Author"

  logger.info("Found " + (activities \ "Activity").length + " activities")

  /**
    * Extract all activities into individual files.
    */
  def extractByActivity(): Unit = {
    for (activity <- activities \ "Activity") {
      val id: String = (activity \ "Id").text

      logger.info(s"Extracting activity $id")

      // Need to replace colons in the timestamp since they are invalid in windows.
      writeFile(id.replace(':', '-'), garminDataForActivities(activity))
    }
  }

  /**
    * Extract all activities into files no greater than the provided maximum size.
    * @param maximumSize The maximum file size.
    */
  def extractBySize(maximumSize: Int): Unit = {
    val activityBuffer = mutable.ListBuffer.empty[Node]
    var fileNumber = 1

    def fileSize(activities: List[Node]): Int = {
      garminDataForActivities(activityBuffer: _*).toString().length
    }

    def writeBuffer(): Unit = {
      val fileName = s"$ExtractBySizeFilePrefix-$fileNumber"

      logger.info(s"Writing " + activityBuffer.size + s" activities to $fileName")

      writeFile(
        fileName,
        garminDataForActivities(activityBuffer: _*)
      )
      activityBuffer.clear()
      fileNumber += 1
    }

    for (activity <- activities \ "Activity") {

      logger.info("Extracting activity " + (activity \ "Id").text)

      // If the current activity will cause the buffer to become larger than `maximumSize` we need
      // to move to a new file, otherwise the activity should be added to the buffer.
      if (fileSize((activityBuffer ++ activity).toList) >= maximumSize) {
        writeBuffer()
        activityBuffer ++= activity
      } else {
        activityBuffer ++= activity
      }
    }

    // If we have reached the last activity, do a final flush of the buffer.
    if (activityBuffer.nonEmpty) {
      writeBuffer()
    }
  }

  /**
    * Get the Garmin data for the provided activities.  This will re-build the garmin tcx file using
    * the existing root level elements and only replacing the activities list.
    * @param newActivities The activities that are being replaced.
    * @return An XML element.
    */
  private def garminDataForActivities(newActivities: Node*) = Elem(
    null,
    "TrainingCenterDatabase",
    Null,
    xml.scope,
    minimizeEmpty = true,
    folders.head,
    Elem(
      null,
      "Activities",
      Null,
      activities.head.scope,
      minimizeEmpty = true,
      newActivities: _*
    ),
    workouts.head,
    courses.head,
    author.head
  )

  /**
    * Write a new Garmin file with the provided file name, and with the provided activities.
    * The fileName should not include the path or extension.
    * @param fileName The name of the file to write.
    * @param data The Garmin data to write in the new file.
    */
  private def writeFile(fileName: String, data: Elem): Unit = {
    val outputPath =
      (outputLocation.toAbsolutePath.toString
        + outputLocation.getFileSystem.getSeparator
        + fileName
        + OutputFileExtension)

    XML.save(
      outputPath,
      data,
      "UTF-8",
      xmlDecl = true
    )
  }
}
